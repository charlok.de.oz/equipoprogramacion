﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
//using System.Drawing.Imaging;

namespace PruebaFirma1
{
    public partial class Form1 : Form
    {
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();


        public Form1()
        {
            InitializeComponent();
            //saveFileDialog1.Filter = "Png Image|*.png|Bitmap Image|*.bmp";
        }
        float PointX = 0;
        float PointY = 0;

        float LastX = 0;
        float LastY = 0;
        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics G = panel1.CreateGraphics();
            G.DrawLine(Pens.Black, PointX, PointY,LastX,LastY);
            LastX = PointX;
            LastY = PointY;
        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            LastX = e.X;
            LastY = e.Y;
        }

        private void Panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PointX = e.X;
                PointY = e.Y;
                Panel1_Paint(this, null);
            }
        }

        ///////////////////////////////Guardar////////////////////////////
        private Bitmap CaptureControl(Control control)
        {
            Size s = control.Size;
            Bitmap memoryImage;
            using (Graphics myGraphics = CreateGraphics())
            {
                memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            }
            using (Graphics memoryGraphics = Graphics.FromImage(memoryImage))
            {
                Point screenPoint = PointToScreen(control.Location);
                memoryGraphics.CopyFromScreen(screenPoint.X, screenPoint.Y, 0, 0, s);
            }
            return memoryImage;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

           // saveFileDialog1.FilterIndex = "C:\Users\Charlok\source\repos\6to\PruebaFirma1";
            saveFileDialog1.Filter = "Png Image(*.png)|*.png";
            //saveFileDialog1.DefaultExt="C:/Users/Charlok/source/repos/6to/PruebaFirma1";
            //Si el usuario no presiona cancelar, proceder a recuperar el nombre del archivo.
            
            saveFileDialog1.ShowDialog();
            string nombreArchivo = saveFileDialog1.FileName;
            using (Bitmap bmp = CaptureControl(panel1))
                {
                    bmp.Save(nombreArchivo, ImageFormat.Png);
                }
            
        }
            
    }
}
