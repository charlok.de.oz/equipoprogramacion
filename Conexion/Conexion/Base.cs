﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Conexion
{
    public class Base
    {
        MySqlConnection conn;
        public Base(string s, string u, string pd, string bd)
        {
            conn = new MySqlConnection("server="+s+";user="+u+";password="+pd+"; database="+bd);
        }
        public string comando(string sql)
        {
            string r = "";
            try
            {
                MySqlCommand c = new MySqlCommand(sql,conn);
                conn.Open();
                c.ExecuteNonQuery();
                conn.Close();
                r = "correcto";
            }
            catch (Exception ex)
            {
                conn.Close();
                r = ex.Message;
            }
            return r;
        }
        public DataSet consultar(string sql,string tabla)
        {
            DataSet ds = new DataSet();
            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(sql, conn);
                conn.Open();
                da.Fill(ds, tabla);
                conn.Close();
                return ds;
            }
            catch (Exception)
            {
                conn.Close();
                return ds;
            }
        }

    }
}
