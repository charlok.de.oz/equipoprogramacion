﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace ConexionSQLite
{
    public class Base
    {
        SQLiteConnection con;
        public Base(string r)
        {
            con = new SQLiteConnection(r);
        }
        public string comando(string sqlite)
        {
            string r = "";
            try
            {
                SQLiteCommand c = new SQLiteCommand(sqlite, con);
                con.Open();
                c.ExecuteNonQuery();
                con.Close();
                r = "correcto";
            }
            catch (Exception ex)
            {
                con.Close();
                r = ex.Message;
            }
            return r;
        }
        public DataSet consultar(string sqlite, string tabla)
        {
            DataSet ds = new DataSet();
            try
            {
                SQLiteDataAdapter da = new SQLiteDataAdapter(sqlite, con);
                con.Open();
                da.Fill(ds, tabla);
                con.Close();
                return ds;
            }
            catch (Exception)
            {
                con.Close();
                return ds;
            }
        }
    }
}
